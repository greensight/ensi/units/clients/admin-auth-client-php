# Ensi\AdminAuthClient\EnumsApi

All URIs are relative to *http://localhost/api/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getRightsAccess**](EnumsApi.md#getRightsAccess) | **GET** /enums/rights-access | Получение справочника прав доступа



## getRightsAccess

> \Ensi\AdminAuthClient\Dto\RightsAccessResponse getRightsAccess()

Получение справочника прав доступа

Получение справочника прав доступа

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


$apiInstance = new Ensi\AdminAuthClient\Api\EnumsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);

try {
    $result = $apiInstance->getRightsAccess();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling EnumsApi->getRightsAccess: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**\Ensi\AdminAuthClient\Dto\RightsAccessResponse**](../Model/RightsAccessResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../../README.md#documentation-for-models)
[[Back to README]](../../README.md)

