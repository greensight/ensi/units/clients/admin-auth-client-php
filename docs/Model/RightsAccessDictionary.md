# # RightsAccessDictionary

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**section** | **string** | Подраздел | [optional] 
**items** | [**\Ensi\AdminAuthClient\Dto\RightsAccess[]**](RightsAccess.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


