# # UserReadonlyProperties

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Идентификатор пользователя | 
**full_name** | **string** | Полное ФИО | 
**short_name** | **string** | Сокращенное ФИО | 
**created_at** | [**\DateTime**](\DateTime.md) | Дата регистрации | 
**updated_at** | [**\DateTime**](\DateTime.md) | Дата обновления | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


