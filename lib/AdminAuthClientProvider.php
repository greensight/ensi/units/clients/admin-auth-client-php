<?php

namespace Ensi\AdminAuthClient;

class AdminAuthClientProvider
{
    /** @var string[] */
    public static $apis = [
        '\Ensi\AdminAuthClient\Api\RolesApi',
        '\Ensi\AdminAuthClient\Api\EnumsApi',
        '\Ensi\AdminAuthClient\Api\OauthApi',
        '\Ensi\AdminAuthClient\Api\UsersApi',
    ];

    /** @var string[] */
    public static $dtos = [
        '\Ensi\AdminAuthClient\Dto\UserReadonlyProperties',
        '\Ensi\AdminAuthClient\Dto\RightsAccess',
        '\Ensi\AdminAuthClient\Dto\User',
        '\Ensi\AdminAuthClient\Dto\ErrorResponse',
        '\Ensi\AdminAuthClient\Dto\SearchUsersRequest',
        '\Ensi\AdminAuthClient\Dto\PaginationTypeCursorEnum',
        '\Ensi\AdminAuthClient\Dto\UserWriteOnlyProperties',
        '\Ensi\AdminAuthClient\Dto\SearchUsersResponseMeta',
        '\Ensi\AdminAuthClient\Dto\SearchRolesResponse',
        '\Ensi\AdminAuthClient\Dto\RequestBodyPagination',
        '\Ensi\AdminAuthClient\Dto\ResponseBodyOffsetPagination',
        '\Ensi\AdminAuthClient\Dto\DeleteRoleFromUserRequest',
        '\Ensi\AdminAuthClient\Dto\PatchRoleRequest',
        '\Ensi\AdminAuthClient\Dto\RightsAccessDictionary',
        '\Ensi\AdminAuthClient\Dto\RoleResponse',
        '\Ensi\AdminAuthClient\Dto\CreateTokenRequest',
        '\Ensi\AdminAuthClient\Dto\UserRoleAllOf',
        '\Ensi\AdminAuthClient\Dto\RequestBodyOffsetPagination',
        '\Ensi\AdminAuthClient\Dto\SearchRolesRequest',
        '\Ensi\AdminAuthClient\Dto\RoleEnum',
        '\Ensi\AdminAuthClient\Dto\UserIncludes',
        '\Ensi\AdminAuthClient\Dto\RoleReadonlyProperties',
        '\Ensi\AdminAuthClient\Dto\AddRolesToUserRequest',
        '\Ensi\AdminAuthClient\Dto\ResponseBodyPagination',
        '\Ensi\AdminAuthClient\Dto\PaginationTypeEnum',
        '\Ensi\AdminAuthClient\Dto\ModelInterface',
        '\Ensi\AdminAuthClient\Dto\CreateRoleRequest',
        '\Ensi\AdminAuthClient\Dto\Error',
        '\Ensi\AdminAuthClient\Dto\UserFillableProperties',
        '\Ensi\AdminAuthClient\Dto\UserResponse',
        '\Ensi\AdminAuthClient\Dto\EmptyDataResponse',
        '\Ensi\AdminAuthClient\Dto\GrantTypeEnum',
        '\Ensi\AdminAuthClient\Dto\MessageForSetPassword',
        '\Ensi\AdminAuthClient\Dto\MessageAboutUpdationUser',
        '\Ensi\AdminAuthClient\Dto\Role',
        '\Ensi\AdminAuthClient\Dto\SearchUsersResponse',
        '\Ensi\AdminAuthClient\Dto\MessageAboutDeactivationUser',
        '\Ensi\AdminAuthClient\Dto\RightsAccessEnum',
        '\Ensi\AdminAuthClient\Dto\ErrorResponse2',
        '\Ensi\AdminAuthClient\Dto\PatchUserRequest',
        '\Ensi\AdminAuthClient\Dto\RoleFillableProperties',
        '\Ensi\AdminAuthClient\Dto\PatchUserRequestAllOf',
        '\Ensi\AdminAuthClient\Dto\CreateUserRequest',
        '\Ensi\AdminAuthClient\Dto\ResponseBodyCursorPagination',
        '\Ensi\AdminAuthClient\Dto\RightsAccessResponse',
        '\Ensi\AdminAuthClient\Dto\MassChangeActiveRequest',
        '\Ensi\AdminAuthClient\Dto\UserRole',
        '\Ensi\AdminAuthClient\Dto\CreateTokenResponse',
        '\Ensi\AdminAuthClient\Dto\RequestBodyCursorPagination',
        '\Ensi\AdminAuthClient\Dto\PaginationTypeOffsetEnum',
    ];

    /** @var string */
    public static $configuration = '\Ensi\AdminAuthClient\Configuration';
}
